https://learn.sparkfun.com/tutorials/how-to-install-an-attiny-bootloader-with-virtual-usb/create-an-arduino-board-definition

in your arduino sketch folder (often Documents/Arduino)
add a folder called hardware (if it doesn’t exist
then, inside hardware, create another folder with an arbitrary name (maybe fabBoards?)
then, inside that folder, create another folder named avr
then, inside avr, create a text file named boards.txt with the information below:
    so, on my machine, all of that looks like:         
        Users/Will/Documents/Arduino/hardware/fabBoards/avr/boards.txt

hello.name=hello.arduino, ATmega328P, 5V, 8MHz internal oscillator
hello.upload.protocol=arduino
hello.upload.maximum_size=30720
hello.upload.speed=57600
hello.bootloader.low_fuses=0xE2
hello.bootloader.high_fuses=0xDA
hello.bootloader.extended_fuses=0x07
hello.bootloader.path=arduino:atmega
hello.bootloader.file=ATmegaBOOT_168_atmega328_pro_8MHz.hex
hello.bootloader.unlock_bits=0x3F
hello.bootloader.lock_bits=0x0F
hello.build.mcu=atmega328p
hello.build.f_cpu=8000000L
hello.build.core=arduino:arduino
hello.build.variant=arduino:standard

with just these steps, the board should show up in your list of boards when you close and restart Arduino.

in the new versions of Arduino, they’ve been organizing the boards in a new way with what they call “platforms”. If you want to make more similar boards and organize them under a subheading in the boards menu, you can create a platforms.txt file which names the heading and (optionally) provides additional compiler information.

In this case, it can be as simple as:

name=Fab Boards
version=0.0.1

